﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinding2D{
	public class Node : MonoBehaviour {

        [SerializeField]
		private int xPosition;
        [SerializeField]
        private int yPosition;
        [SerializeField]
        private int costOfMove = 1;
        [SerializeField]
		private bool isWalkable = true; // Remove this variable after prototype works
		private PathManager pathManager;
		private List<Node> neighbours;

		public Node()
		{
			neighbours = new List<Node>();
		}

		public Node(int xPosition, int yPosition)
		{
			neighbours = new List<Node>();
			this.xPosition = xPosition;
			this.yPosition = yPosition;
		}
		
		public void SetXPosition(int xPosition)
		{
			this.xPosition = xPosition;
		}

		public int GetXPosition()
		{
			return this.xPosition;
		}

		public void SetYPosition(int yPosition)
		{
			this.yPosition = yPosition;
		}

		public int GetYPosition()
		{
			return this.yPosition;
		}

		public void SetCostOfMove(int costOfMove)
		{
			this.costOfMove = costOfMove;
		}

		public int GetCostOfMove()
		{
			return costOfMove;
		}

		public void SetIsWalkable(bool isWalkable)
		{
			this.isWalkable = isWalkable;
		}

		public bool GetIsWalkable()
		{
			return isWalkable;
		}

		public List<Node> GetNeighboursList()
		{ 
			return this.neighbours;
		}
		public void SetNeighboursList(List<Node> neighbours)
		{ 
			this.neighbours = neighbours; 
		}

		public void AddToNeighbourNodeList(Node neighbourToAdd)
		{ 
			this.neighbours.Add(neighbourToAdd); 
		}
		public void RemoveFromNeighbourNodeList(Node neighbourToRemove) 
		{ 
			this.neighbours.Remove(neighbourToRemove); 
		}		

		// Testing Purposes
		void OnMouseUp()
    	{
			pathManager = PathManager.GetInstance();
			pathManager.SetTarget(this);
    	}

	}

}