﻿using System.Collections;
using System.Collections.Generic;
using PathFinding2D;
using UnityEngine;

namespace PathFinding2D {

    //Manager class to be used to called the pathfinding
	public class PathManager : MonoBehaviour {

        #region PathManager Instancing
        //simple method to get an instance of the PathManger class that can be used anywere
        private static PathManager instance;

		private PathManager() {}

		public static PathManager GetInstance()
		{
			return instance;
		}
        #endregion

        private Player player; //ref to the player
		private Map walkableNodes; //object containing any and all nodes on the map
		private Node target; //the target poistion were we want to pathfind to
		private DijkstraAlgorithm dijkstraAlgorithm; //ref to the pathfinding algrothim

		void Awake()
		{
			if(instance == null)
			{
				instance = GetComponent<PathManager>();
			}	
		}

		void Start () {
			dijkstraAlgorithm = GetComponent<DijkstraAlgorithm>();

			if(dijkstraAlgorithm == null)
			{
				print("NULL SCRIPT: DijkstraAlgorithm script is null");
			}

			walkableNodes = Map.GetInstance();

			if(walkableNodes == null)
			{
				print("NULL OBJECT: walkableNodes Map is null");
			}

			print("List of nodes = " + walkableNodes.SizeOfList()); 

		}

		void Update()
		{}

		public Player GetPlayer()
		{
			return this.player;
		}

		public void SetPlayer(Player player)
		{
			this.player = player;
			print("Player Set: " + player);
		}

		public Node GetTarget()
		{
			return this.target;
		}

		public void SetTarget(Node target)
		{
			this.target = target;
			print("Target set: " + target);
		}

		public Map GetWalkableNodes()
		{
			return this.walkableNodes;
		} 

        public bool CheckIfNodeExists(Node _node)
        {
            if (dijkstraAlgorithm.CheckIfNodeExsits(_node))
                return true;
            else
                return false;
        }

        public List<Node> GetSpecficArea(int _range,int[] _cords)
        {
            List<Node> AreaList = dijkstraAlgorithm.GetSpecficArea(walkableNodes, _range, _cords);
            return AreaList;
        }
	}

}
