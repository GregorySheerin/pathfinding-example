﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinding2D {

	public interface IGrid
	{
		void BuildWalkableGrid(Map walkableGrid);
	}

}

