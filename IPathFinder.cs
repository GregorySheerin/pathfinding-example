﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinding2D{
	public interface IPathFinder
	{
		void ProcessPath(Map walkableNodes, Player source, Node target);

	} 
	
}