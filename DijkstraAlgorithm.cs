﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace PathFinding2D{
	public class DijkstraAlgorithm : MonoBehaviour, IPathFinder {

        private Map walkableNodes; //object containing all the nodes contained on the map
        private Player player; //ref to player script on any given unit
        private Node target; //target position to pathfind to
        private PathManager pathManager; //ref to the pathfinding manager

        void Start() //currenlty nothing needed on start
        {}

        //method to init the pathfinding
        public void InitalizePathVariables()
        {
            pathManager = PathManager.GetInstance();

            walkableNodes = pathManager.GetWalkableNodes();
            player = pathManager.GetPlayer();
            target = pathManager.GetTarget();

            PopulateNodeNeighbours();

            ProcessPath(walkableNodes, player, target);
        }

        public void ProcessPath(Map walkableNodes, Player source, Node target)
        {
            Dictionary<Node, float> dist = new Dictionary<Node, float>(); //stores a node and distance from current node see below
            Dictionary<Node, Node> prev = new Dictionary<Node, Node>(); //this will evently be the path for the player
            List<Node> UnVistedNodes = new List<Node>();  //list of nodes that the algorthim has yet to get to
            List<Node> CurrentPath = new List<Node>(); //used to store the current path of nodes
            List <Node> walkableNodesList = walkableNodes.GetWalkableList(); //list of all nodes on map

            //First,get the node that the player passed in is standing on
            Node sourceNode = walkableNodesList.FirstOrDefault(x => x.GetXPosition() == source.GetXPosition() && x.GetYPosition() == source.GetYPosition());

            //set the source node values on the distance array to 0 since we are standing on it
            dist[sourceNode] = 0; 

            //set the prev node to null
            prev[sourceNode] = null; 

            //go though all the nodes,check if they are not the source node(if so,set the dist to infinity to avoid it being used in pathfinding)
            //really this is just to rule out the starting node from pathfinding
            foreach (Node V in walkableNodesList)
            {
                if (V != sourceNode)
                {
                    dist[V] = Mathf.Infinity;
                    prev[V] = null;
                }

                UnVistedNodes.Add(V); //add the node to the list
            }
            
            //This will go though every node and its neigbours,calcuating the distance
            //thus calcauting the most effecit possible route to the target point from the start point
            while (UnVistedNodes.Count > 0)
            {

                // u is going to be the unvisted node with the smallest distance to the unit
                Node U = null;
                foreach (Node PossibleU in UnVistedNodes)
                {
                    //check if its null OR if the distance of the looping Possiblue U is lesser than than U
                    if (U == null || dist[PossibleU] < dist[U])
                    {
                        //if it is set U to the possible,since this will be a lower distance than the U before it
                        U = PossibleU;

                    }
                }

                UnVistedNodes.Remove(U); //once we are done remove it from the list

                if (U == target) //check if we have our information(the best path )
                {   
                    break; //if so then break out of the loop
                }

                foreach (Node V in U.GetNeighboursList())
                {
                    //calcuate the distance of U,taking into account the cost of the tile
                    float alt = dist[U] + CostToEnterTile(U.GetXPosition(), U.GetYPosition(), V.GetXPosition(), V.GetYPosition());
                    //if the value is lesser
                    if (alt < dist[V])
                    {
                        dist[V] = alt; //set the distance to what we just calucated
                        prev[V] = U; //assign the value into the previous arrry
                    }
                }
            }

            //once we are here
            //1.we have the shortest route to our target
            //2.there is no route avabile to our target

            if (prev[target] == null)
            {
                //if we get here,there is no route between our target and our source
                Debug.Log("that is impossible!");
                return; //break out of this method
            }

            //set the current node to the target to start working though the prev array
            Node currentNode = target; 

            //the prev array is were our path is located,we have it just need to make it cleaner
            //so we step though the "prev" array and add it to the current path
            while (currentNode != null)
            {
                CurrentPath.Add(currentNode);
                //when the item in the array at this poistion is null,we are finished
                currentNode = prev[currentNode];
            }

            //right now our path describes a route from our target to our source,but its the wrong way around
            //the list describes how to get to our target to us,to fix this,simply invert the list
            CurrentPath.Reverse(); //use linq to invert this 

        }// End process path


        //method to get the Cost on any given node
        public float CostToEnterTile(int SourceX, int SourcecY, int TargetX, int TargetY)
        {
            //get a list of all the nodes
            List<Node> walkableNodesList = walkableNodes.GetWalkableList();
            
            //now query this list to get the exact cost of the node at the Target X/Y position
            Node walkableNode = walkableNodesList.FirstOrDefault(x => x.GetXPosition() == TargetX && x.GetYPosition() == TargetY);
            float CostToEnter = walkableNode.GetCostOfMove();

            if (SourceX != TargetX && SourcecY != TargetY) //check if we are moving diangoly
            {
                //add a tiny bit of cost to tell our pathfinding to prefer straight line movement
                CostToEnter += 0.0001f;
            }
            return CostToEnter; //pass back the movement cost of the tile

        }
 
        //method to get if the Passable Propity on an object is True/flase
        bool CheckIfPassable(int xPosition, int yPosition)
        {
            //get the list of every walkable node
            List<Node> walkableNodesList = walkableNodes.GetWalkableList();

            //get the node at the X and Y postion passed in
            Node walkableNode = walkableNodesList.FirstOrDefault(nodeFromList => nodeFromList.GetXPosition() == xPosition && nodeFromList.GetYPosition() == yPosition);
            return walkableNode.GetIsWalkable(); //return the IsWalkable Proptiy   
        }

        void PopulateNodeNeighbours()
        {
            //get a list of all nodes on the map
            List<Node> walkableNodesList = walkableNodes.GetWalkableList();

            //init the var for a neighbournode,has to be null to start off with
            Node neighbourNode = null; 

            //foreach to check if nodes are neighbouring the current node
            //each if checks to see if a node exists in the theroical position
            //this position would be were a neigbour would be located on a grid
            //but since it is possible to not find a noded in that poistion,we need to check
            foreach (Node node in walkableNodesList)
            {
                //Note:Commented sections are to check for a neigbour in a diangoal postion from the current node
                //uncomment them if we want dignaol movement in our game
                if (CheckIfNodeExsits(new Node(node.GetXPosition(), node.GetYPosition() +1)))
                {
                    neighbourNode = walkableNodesList.First(x => x.GetXPosition() == node.GetXPosition() && x.GetYPosition() == node.GetYPosition() + 1);
                    node.AddToNeighbourNodeList(neighbourNode);
                }

                //if (CheckIfNodeExsits(new Node(node.GetXPosition() +1, node.GetYPosition() +1)))
                //{
                //    neighbourNode = interactableNodes.First(x => x.GetXPosition()  == node.GetXPosition() + 1 && x.GetYPosition() == node.GetYPosition() +1);
                //    node.AddToNeighbourNodeList(neighbourNode);
                //}

                if (CheckIfNodeExsits(new Node(node.GetXPosition() +1, node.GetYPosition())))
                {
                    neighbourNode = walkableNodesList.First(x => x.GetXPosition() == node.GetXPosition() +1 && x.GetYPosition() == node.GetYPosition());
                    node.AddToNeighbourNodeList(neighbourNode);
                }

                //if (CheckIfNodeExsits(new Node(node.GetXPosition() +1, node.GetYPosition() -1)))
                //{
                //    neighbourNode = interactableNodes.First(x => x.GetXPosition()  == node.GetXPosition() + 1 && x.GetYPosition() == node.GetYPosition() -1);
                //    node.AddToNeighbourNodeList(neighbourNode);
                //}

                if (CheckIfNodeExsits(new Node(node.GetXPosition(), node.GetYPosition() -1)))
                {
                    neighbourNode = walkableNodesList.First(x => x.GetXPosition() == node.GetXPosition() && x.GetYPosition() == node.GetYPosition() -1);
                    node.AddToNeighbourNodeList(neighbourNode);
                }

                //if (CheckIfNodeExsits(new Node(node.GetXPosition() -1, node.GetYPosition() -1)))
                //{
                //    neighbourNode = interactableNodes.First(x => x.GetXPosition() == node.GetXPosition() - 1 && x.GetYPosition() == node.GetYPosition() -1);
                //    node.AddToNeighbourNodeList(neighbourNode);
                //}

                if (CheckIfNodeExsits(new Node(node.GetXPosition() -1, node.GetYPosition())))
                {
                    neighbourNode = walkableNodesList.First(x => x.GetXPosition() == node.GetXPosition() - 1 && x.GetYPosition() == node.GetYPosition());
                    node.AddToNeighbourNodeList(neighbourNode);
                }

                //if (CheckIfNodeExsits(new Node(node.GetXPosition() -1, node.GetYPosition() +1)))
                //{
                //    neighbourNode = interactableNodes.First(x => x.GetXPosition() == node.GetXPosition() - 1 && x.GetYPosition() == node.GetYPosition() +1);
                //    node.AddToNeighbourNodeList(neighbourNode);
                //}

            }

        }

        //Method to check if a node exists in the first place
        public bool CheckIfNodeExsits(Node node)
        {
            //get a list of all the nodes
            List<Node> walkableNodesList = walkableNodes.GetWalkableList();

            //check if that node is in the list,and return the result
            if (walkableNodesList.Any(x => x.GetXPosition() == node.GetXPosition() && x.GetYPosition() == node.GetYPosition()))
                return true;
            else
                return false;
        }  
        
        //Method to get a Area around a set of cords
        //pulling them in as an array of 2 ints,this way we can use this for many functions
        //area of affects abilites,scaning for Units,fog of war mechiancs ect
        public List<Node> GetSpecficArea(Map _walkAbleNodes,int _range,int[] _cords)
        {  
            //first init the return list
            List<PathFinding2D.Node> Area = new List<PathFinding2D.Node>();
            //then get a list of all nodes
            List<PathFinding2D.Node> allnodes = _walkAbleNodes.GetWalkableList();

            //create a null node to use below
            PathFinding2D.Node possible = null;

            //nested loop to calucate the area,we can one y level for all nodes on it,and then onto the next
            //until both X and Y are greater than the ranage passed
            int y = 0;
            while (y < _range + 1)
            {
                for (int X = 0; X < _range + 1; X++)
                {
                    //this follows the same format as our walkable nodes calucations
                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] && x.GetYPosition() == _cords[1] + y);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null; //reset this after every check,since we need to check if it returns null from the all Nodes list

                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] + X && x.GetYPosition() == _cords[1] + y);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null;

                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] + X && x.GetYPosition() == _cords[1]);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null;

                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] + X && x.GetYPosition() == _cords[1] - y);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null;

                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] && x.GetYPosition() == _cords[1] - y);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null;

                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] - X && x.GetYPosition() == _cords[1] - y);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null;

                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] - X && x.GetYPosition() == _cords[1]);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null;

                    possible = allnodes.FirstOrDefault(x => x.GetXPosition() == _cords[0] - X && x.GetYPosition() == _cords[1] + y);
                    if (possible != null)
                    {
                        Area.Add(possible);
                    }
                    possible = null;
                }
                y++;
            }

            //return the area
            return Area;

        }

        

    }

}
