﻿using System.Collections;
using System.Collections.Generic;

namespace PathFinding2D{

	/*
		Map object contains a list which holds the tiles used for calculating walkable paths
		This class is a singleton as we will only ever want one version of the list
	*/
	public class Map {

		private static Map instance;

		private List<Node> walkable;

   		private Map() {}

		public static Map GetInstance()
		{
			
			if (instance == null)
			{
				instance = new Map();
			}

			return instance;
			
		}

		public void InitialzeList()
		{
			this.walkable = new List<Node>();
		}

		public int SizeOfList()
		{
			return this.walkable.Count;
		}

		public void AddNode(Node node)
		{
			walkable.Add(node);
		}

		public void RemoveAllNodesFromList()
		{
			walkable.Clear();
		}

		public List<Node> GetWalkableList()
		{
			return this.walkable;
		}

	}

}
