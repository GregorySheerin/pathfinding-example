﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
	A stage is the same concept as a level.
	A grid is contained in a stage and is what displays all tiles
	A map is derived from a grid, it contains the tiles which are walkable
 */
namespace PathFinding2D {

	public class Stage : MonoBehaviour, IGrid{

		public Grid grid; //ref to the grid,assigned via the editor
		public Map walkableGrid; //ref to the object containing all nodes
		private List<Node> walkableNodes; //list to put the nodes into

        void Start () {

			if(grid == null)
			{
				print("NULL OBJECT: Please attach a grid to the Stage script in the inspector");
			}

			walkableGrid = Map.GetInstance();

			if(walkableGrid == null)
			{
				print("NULL OBJECT: Map object was not retrieved");
			}

			walkableGrid.InitialzeList();
			walkableNodes = walkableGrid.GetWalkableList();

			if(walkableNodes == null)
			{
				print("NULL LIST: List for walkable nodes not retrieved");
			}
            
			BuildWalkableGrid(walkableGrid);

		}

        //Method to assign data to each node on the map
        public void BuildWalkableGrid(Map walkableGrid)
        {
            //create and get all game objects that are nodes for pathfinding on the editor
			List<GameObject> walkableGameObjects = new List<GameObject>();
			walkableGameObjects = GameObject.FindGameObjectsWithTag("Walkable").ToList();

			if(walkableNodes == null)
			{
				print("NULL LIST: List for walkable game objects not initalized");
			}

            //go though every object we found on the level,and set their X and Y positions to that on the grid
            //doing this allows us simple access to the X and Y positions for later use
			Node gameObjectNode;
			if(walkableGameObjects != null)
        	{
            
				foreach (GameObject gameObject in walkableGameObjects)
				{
					
					gameObjectNode = gameObject.GetComponent<Node>();

					if(gameObjectNode == null)
					{
						print("NULL OBJECT: gameObjectNode is not initialized");
					}

					gameObjectNode.SetXPosition(grid.WorldToCell(gameObject.transform.position).x);
                	gameObjectNode.SetYPosition(grid.WorldToCell(gameObject.transform.position).y);
                    gameObjectNode.SetCostOfMove(1);//defualt cost 
					walkableGrid.AddNode(gameObjectNode);

				}

        	}

        }

		void OnMouseUp()
		{
			print("GET NODE VALUES: X = " + grid.ToString());
		}

	}
}
